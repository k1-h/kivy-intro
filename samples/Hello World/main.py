# my.py
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout


class MainApp(App):
    def build(self):
        return HelloWorld()

class HelloWorld(BoxLayout):
    def change_text(self):
        self.ids.label.text = 'Hello Wolrd!'

MainApp().run()
