from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button


class Dictionary(GridLayout):
    def __init__(self, **kwargs):
        super(Dictionary, self).__init__(**kwargs)
        company_info = {
            'Google': {'founded_year': 1998, 'ceo': 'Larry Page', 'employees': 55030},
            'Microsoft': {'founded_year': 1975, 'ceo': 'Satya Nadella', 'employees': 128076},
            'Apple': {'founded_year': 1976, 'ceo': 'Tim Cook', 'employees': 98000},
            'Facebook': {'founded_year': 2004, 'ceo': 'Mark Zuckerberg', 'employees': 8348},
        }
        self.cols = 8
        for company, info in company_info.items():
            self.add_widget(Label(text='Name: '))
            self.add_widget(Label(text=company))
            self.add_widget(Label(text='Founded Year: '))
            self.add_widget(Label(text=str(info['founded_year'])))
            self.add_widget(Label(text='CEO: '))
            self.add_widget(Label(text=info['ceo']))
            self.add_widget(Label(text='Employees: '))
            self.add_widget(Label(text=str(info['employees'])))

class MyApp(App):
    def build(self):
        return Dictionary()

if __name__ == '__main__':
    MyApp().run()
